package com.twuc.webApp.controller;

import com.twuc.webApp.bean.Student;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    @PostMapping("/api/response")
    @ResponseStatus
    public ResponseEntity<String> getString() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("sid", "a111");

        return new ResponseEntity<>("Custom header set", headers, HttpStatus.CREATED);
    }

    @PostMapping("/api/student")
    public ResponseEntity<Student> getStudent(@RequestBody Student student) {
        return new ResponseEntity<Student>(student, HttpStatus.CREATED);
    }
}

package com.twuc.webApp.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueryStringController {

    @GetMapping("/api/query/string")
    public String getAge(@RequestParam String age) {
        return "age-24";
    }

    @GetMapping("/api/query/string/color")
    public String getName(@RequestParam("!name") String age) {
        return "age-24";
    }


}

package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/api/users/{id}/books")
    public String getUserBook(@PathVariable Long id) {
        return "The book for user" + id;
    }

    @GetMapping("/api/users/2/books")
    public String getUserBook2(@PathVariable Long id) {
        return 2 + " The book for user" + id;
    }

    @GetMapping("/api/users")
    public String getUserClassAndGender(@RequestParam String clazz, @RequestParam String gender) {
        return clazz + "===" + gender;
    }

    @GetMapping("/api/segments/good")
    public String testRoute1() {
        return "testRoute1 == good";
    }

    @GetMapping("/api/segments/{segmentName}")
    public String testRoute2() {
        return "testRoute2 == good";
    }

    @GetMapping("/api/user/?/info")
    public String getUserInfoWithId() {
        return "user info";
    }

    @GetMapping("/api/wildcards/*")
    public String testStarCharOnTail() {
        return "star char";
    }

    @GetMapping("/api/*/books")
    public String testStarCharOnMiddle() {
        return "star char";
    }

    @GetMapping("/api/wildcard/before/*/after")
    public String testStarCharWhenInputPartPath() {
        return "star char";
    }

    @GetMapping("/api/cart/*category")
    public String testStarCharWhenStarMappingPrefix() {
        return "star char";
    }

    @GetMapping("/api/cart/book*")
    public String testStarCharWhenStarMappingSuffix() {
        return "star char";
    }

    @GetMapping("/api/cart/**/bookcategory")
    public String testManyStarCharMapping() {
        return "star char";
    }

    @GetMapping(value = "/apii/info/{id:[0-9]}")
    public String testRegexMapping(@PathVariable("id") Integer id) {
         return "star char " + id;
    }


}

package com.twuc.webApp.bean;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Student {

    private String name;
    private String age;
    @JsonSetter("phone_number")
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    @JsonGetter("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

}

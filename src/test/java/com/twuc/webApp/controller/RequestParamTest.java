package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestParamTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_class_gender_as_response() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/users?clazz=eee&gender=male")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("eee===male");
    }
}

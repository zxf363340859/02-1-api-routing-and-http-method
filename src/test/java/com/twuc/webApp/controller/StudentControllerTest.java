package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void test_response_entity() throws Exception {
        mockMvc.perform(post("/api/response").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(header().exists("sid"))
                .andExpect(status().is(201));
    }



    @Test
    void should_return_student_have_name_age_phone() throws Exception {
        mockMvc.perform(post("/api/student").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{\n" +
                "\t\"name\": \"zxf\",\n" +
                "\t\"age\": \"24\",\n" +
                "\t\"phone_number\": \"666666\"\n" +
                "}")).andExpect(content().string("{\"name\":\"zxf\",\"age\":\"24\",\"phone_number\":\"666666\"}"));
    }

    @Test
    void should_return_student_have_name_phone() throws Exception {
        mockMvc.perform(post("/api/student").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("{\n" +
                "\t\"name\": \"zxf\",\n" +
                "\t\"phone_number\": \"666666\"\n" +
                "}")).andExpect(content().string("{\"name\":\"zxf\",\"phone_number\":\"666666\"}"));
    }

}

package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_as_response_status() throws Exception {
        int status = mockMvc.perform(get("/api/users/2/books")).andReturn().getResponse().getStatus();
        String contentType = mockMvc.perform(get("/api/users/2/books")).andReturn().getResponse().getContentType();
        String contentAsString = mockMvc.perform(get("/api/users/2/books")).andReturn().getResponse().getContentAsString();
        assertThat(status).isEqualTo(200);
        assertThat(contentType).isEqualTo("text/plain;charset=UTF-8");
        assertThat(contentAsString).isEqualTo("The book for user2");

    }

    @Test
    void prove_pathvariable_and_uri_uppercase_lowercase() throws Exception {
        int status = mockMvc.perform(get("/api/users/2/BOOKS")).andReturn().getResponse().getStatus();
        assertThat(status).isNotEqualTo(200);
    }


    @Test
    void prove_route_prority() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/segments/good")).andReturn().getResponse().getContentAsString();
        assertThat(contentAsString).isEqualTo("testRoute1 == good");
    }

    @Test
    void test_one_char_mapping_question_input_more_than_one_char() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/user/aa/info")).andReturn().getResponse().getContentAsString();
        mockMvc.perform(get("/api/user/aa/info")).andExpect(status().is4xxClientError());
    }

    @Test
    void test_one_char_mapping_question_input_less_than_one_char() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/user//info")).andReturn().getResponse().getContentAsString();
        mockMvc.perform(get("/api/user//info")).andExpect(status().is4xxClientError());
    }

    @Test
    void test_star_char_mapping_question_when_star_char_at_tail_path() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_star_char_mapping_question_when_star_char_at_middle_path() throws Exception {
        mockMvc.perform(get("/api/mike/books")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_part_path_request_controller_success() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/aaa/after")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_part_path_request_controller_fail() throws Exception {
        mockMvc.perform(get("/api/before/after")).andExpect(status().is4xxClientError());
    }

    @Test
    void test_star_char_mapping_question_when_star_char_mapping_prefix() throws Exception {
        mockMvc.perform(get("/api/cart/bookcategory")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_star_char_mapping_question_when_star_char_mapping_suffix() throws Exception {
        mockMvc.perform(get("/api/cart/bookcategory")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_many_star_char_mapping_question() throws Exception {
        mockMvc.perform(get("/api/cart/order/pay/bookcategory")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_regex_mapping_path_return_fail() throws Exception {
        mockMvc.perform(get("/apii/info/aaaaaa11+/")).andExpect(status().is2xxSuccessful());
    }

    @Test
    void test_regex_mapping_path_success() throws Exception {
        mockMvc.perform(get("/apii/info/1")).andExpect(status().is2xxSuccessful());
    }
}

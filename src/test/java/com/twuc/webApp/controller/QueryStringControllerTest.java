package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class QueryStringControllerTest {

    @Autowired
    private MockMvc mockMvc;

    //2.8.1
    @Test
    void test_use_query_string() throws Exception {
        mockMvc.perform(get("/api/query/string").param("age", "24"))
                .andExpect(content().string("age-24"));
    }

    @Test
    void test_use_query_string_but_not_param() throws Exception {
        mockMvc.perform(get("/api/query/string"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void test_use_query_string_other() throws Exception {
        mockMvc.perform(get("/api/query/string/color").param("color", "red").param("age", "24"))
                .andExpect(status().is4xxClientError());
    }
}
